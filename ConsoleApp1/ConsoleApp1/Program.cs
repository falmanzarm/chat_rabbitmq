﻿using RabbitMQ.Client;
using System;
using System.Text;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Introduzca el nombre o direccion IP del servidor: ");
            string _hostName = Console.ReadLine();

            try
            {
                var factory = new ConnectionFactory()
                {
                    UserName = "admin",
                    Password = "admin",
                    HostName = _hostName
                };
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        Console.Write("\nIntroduzca el nombre de la cola: ");
                        string queue = Console.ReadLine();
                        channel.QueueDeclare(queue, false, false, false, null);


                        Console.WriteLine("\nDigite  0 - Cero para salir...");
                        string message = string.Empty;

                        do
                        {
                            Console.WriteLine("\n***********************************");
                            Console.Write("\nIntroduzca el cuerpo del  mensaje: ");
                            message = Console.ReadLine();
                            var body = Encoding.UTF8.GetBytes(message);

                            if (!message.Equals("0"))
                            {
                                channel.BasicPublish("",queue, false, null, body);
                            }
                        } while (!message.Equals("0"));

                        Console.WriteLine("\nPrecione cualquier tecla para terminar el programa...");
                        Console.ReadKey();
                    }
                }
            }
            catch (Exception EX)
            {
                Console.WriteLine(EX.Message);
                Console.ReadKey();
            }            
        }
    }
}
