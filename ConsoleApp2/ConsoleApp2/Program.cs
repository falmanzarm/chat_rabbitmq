﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.Text;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Introduzca el nombre del servidor: ");
            string _hostName = Console.ReadLine();
            Console.Write("Introduzca el nombre de la cola: ");
            string queue = Console.ReadLine();
            var factory = new ConnectionFactory()
            {
                UserName = "admin",
                Password = "admin",
                HostName = _hostName
            };

            try
            {
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.QueueDeclare(queue, false, false, false, null);
                        var consumer = new QueueingBasicConsumer(channel);
                        channel.BasicConsume(queue, true, consumer);

                        Console.WriteLine("\nEsperando mensajes...");
                        while (true)
                        {
                            var ea = (BasicDeliverEventArgs)consumer.Queue.Dequeue();
                            var body = ea.Body;
                            var message = Encoding.UTF8.GetString(body);
                            Console.WriteLine("Recibido: {0}", message);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.ReadKey();
            }
        }
    }
}
